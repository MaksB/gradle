package epam.maven.build;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class FileCreator {

	private static final String NAME_DIRECTORY_PROPERTIES = "nameDirectory.properties";
	private static final String LOCATION = "location";
	private static List<String> filePatch = new ArrayList<String>();

	private String[] getDirectoryName() {
		Properties props = new Properties();
		try {
			props.load(getClass().getClassLoader().getResourceAsStream(NAME_DIRECTORY_PROPERTIES));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props.getProperty(LOCATION).split(",");
	}

	public void createFile() {
		for (String string : getDirectoryName()) {
			new File(string).mkdirs();
			try {
				String fileName = string + "\\" + new Random().nextInt() + ".txt";
				File file = new File(fileName);
				file.createNewFile();
				filePatch.add(file.getPath());
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public static List<String> getFilePatch() {
		return filePatch;
	}

}
