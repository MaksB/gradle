package epam.maven.test;

import org.junit.Test;

import epam.maven.build.FileCreator;
import static org.mockito.Mockito.*;


import static org.junit.Assert.*;

public class FileCreatorTest {

	@Test
	public void shouldRunMethodCreateFileTest() {
		FileCreator fileCreator = mock(FileCreator.class);
		fileCreator.createFile();
		verify(fileCreator).createFile();
	}

	@Test
	public void shouldCreateFileTwoTimesTest() {
		FileCreator fileCreator = mock(FileCreator.class);
		fileCreator.createFile();
		fileCreator.createFile();
		verify(fileCreator, times(2)).createFile();
	}
	
	@Test
	public void shouldReturnFilePatchTest() {
		FileCreator fileCreator = new FileCreator();
		fileCreator.createFile();
		assertTrue("Tets not empty list after create file",fileCreator.getFilePatch() .size() > 0);
	}

}
