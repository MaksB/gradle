package epam.main.service;

import java.util.List;

public interface FileService {

	void createFile();
	void deleteFile(String path);
	List<String> getFileList();
}
