package epam.main.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import epam.ant.delete.DeleteFile;
import epam.main.service.FileService;
import epam.maven.build.FileCreator;

@Service
public class FileServiceImpl implements FileService{

	private FileCreator creator;
	private DeleteFile delete; 
	public FileServiceImpl() {
	 creator = new FileCreator();
	 delete = new DeleteFile();
	}
	
	@Override
	public void createFile() {
		creator.createFile();
		
	}
	
	@Override
	public void deleteFile(String path) {
		delete.delete(path);
	}
	
	@Override
	public List<String> getFileList() {
		return creator.getFilePatch();
	}
}
