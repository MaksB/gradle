package epam.main.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import epam.main.service.FileService;

@Controller
public class FileController {

	@Autowired
	private FileService fileService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		fileService.createFile();
		
		ModelAndView model = new ModelAndView();
		model.setViewName("fileCreate");
		
		model.addObject("list",fileService.getFileList());
		
		return model;

	}
	
	@RequestMapping(value = "/delete", params = "path", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam("path") String path) {

		ModelAndView model = new ModelAndView();
		model.setViewName("fileCreate");
		
		fileService.getFileList().remove(path);
		fileService.deleteFile(path);
		model.addObject("list",fileService.getFileList());
		
		return model;

	}

}
